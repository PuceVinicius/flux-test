﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

class MBuildProcessor : IPreprocessBuildWithReport
{
    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildReport report)
    {
        Debug.Log("MyCustomBuildProcessor.OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);
    }

}
#endif

public static class Stick {

    public enum stck {ONE_METHOD, MANESTIC }

    public static stck GetStck()
    {
        return Application.platform == RuntimePlatform.WindowsEditor ? stck.MANESTIC : stck.ONE_METHOD;
    }

}

