using UnityEngine;
using Gaminho;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gaminho {
    public static class SaveData {

        public static void SaveGame(GameData gameData) {
            
            string path = Application.persistentDataPath + "/gamedata.flux";

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Create);
            
            formatter.Serialize(stream, gameData);
            stream.Close();
        }

        public static GameData LoadGame() {

            string path = Application.persistentDataPath + "/gamedata.flux";
            
            if (File.Exists(path)) {

                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                
                GameData gameData = formatter.Deserialize(stream) as GameData;
                stream.Close();
                
                return gameData;
            }
            else {
                
                Debug.Log("Couldn't find save file!");
                return null;
            }
        }
    }
}
