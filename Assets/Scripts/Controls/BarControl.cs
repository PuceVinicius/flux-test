using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarControl : MonoBehaviour {
    public int maxValue = 10;
    public int initialValue = 10;
    public Transform background;
    public Transform bar; 
    public Transform followObject;

    private int currentValue;
    private Vector3 originalPosition;
    public Vector3 offset = new Vector3(0, 60, 0);
    

    private void LateUpdate() {
        if(followObject != null)
            transform.localPosition = followObject.localPosition - originalPosition + offset;
    }

    private void Start() {
        if(followObject != null)
            originalPosition = followObject.localPosition;
    }

    public void ShowBar() {
        background.gameObject.SetActive(true);
        bar.gameObject.SetActive(true);
    }

    public void HideBar() {
        background.gameObject.SetActive(false);
        bar.gameObject.SetActive(false);
    }

    public void ResetBar() {
        currentValue = initialValue;
        bar.localScale = new Vector3(currentValue / maxValue, 1, 1);
    }

    public void UpdateBar(int deltaValue) {
        Debug.Log($"deltaValue: {deltaValue}");
        currentValue += deltaValue;
        Debug.Log($"currentValue: {currentValue}");
        bar.localScale = new Vector3((float)currentValue / (float)maxValue, 1, 1);
    }
}
