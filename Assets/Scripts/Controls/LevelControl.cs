using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

public class LevelControl : MonoBehaviour {
    
    public void SaveCurrentLevel() {
        GameData gameData = new GameData(Statics.CurrentLevel);
        SaveData.SaveGame(gameData);
    }

    public void LoadLevel() {
        GameData gameData = SaveData.LoadGame();

        if(gameData != null)    Statics.CurrentLevel = gameData.level;
        else                    Statics.CurrentLevel = 0;
    }

    public void ResetLevel() {

        Statics.WithShield = false;
        Statics.EnemiesDead = 0;
        Statics.Points = 0;
        Statics.ShootingSelected = 2;
    }
}
