using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseControl : MonoBehaviour {

    public void PauseGame() {

        Time.timeScale = 0;
        AudioListener.pause = true;
    }

    public void ResumeGame() {

        Time.timeScale = 1;
        AudioListener.pause = false;
    }
}
