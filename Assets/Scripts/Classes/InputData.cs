using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Gaminho {
    [System.Serializable]
    public class InputData {
        
        public string key;        
        public float value;
        public DateTime timePressed;

        public InputData (string newKey, float newValue, DateTime newTime) {
            key = newKey;
            value = newValue;
            timePressed = newTime;
        }
    }
}
