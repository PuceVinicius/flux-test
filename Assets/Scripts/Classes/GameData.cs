using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gaminho {
    [System.Serializable]
    public class GameData {
        
        public int level;

        public GameData (int newLevel) {
            level = newLevel;
        }
    }
}
